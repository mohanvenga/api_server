const request = require('supertest');
const _ = require('lodash');
jest.mock('./dbFunctions');
const { getRecipients } = require('./dbFunctions');
const app = require('./app')

describe('Test /api/retrievefornotifications', () => {
  test('Should return 200 response code recipients array in json object response', () => {
      return request(app)
      .post("/api/retrievefornotifications")
      .send({
        "teacher":  "teacherken@example.com",
        "notification": "Hello students! @studentjon@example.com @studenthon@example.com"
      })
      .then(response => {
          expect(response.statusCode).toBe(200);
          expect(_.isArray(response.body.recipients)).toBe(true);
      })
  });
  test('Should handle internal server db function errors', () => {
    getRecipients.mockImplementation(() => {
      const promise1 = new Promise(function(resolve, reject) {
        throw 'Uh-oh!';
      });
      return promise1;
    });
      return request(app)
      .post("/api/retrievefornotifications")
      .send({
        "teacher":  "teacherken@example.com",
        "notification": "Hello students! @studentjon@example.com @studenthon@example.com"
      })
      .then(response => {
          expect(response.statusCode).toBe(500);
          expect(response.body.message).toEqual('Processing error. Please try again later.');
      })
  });
  test('Should return 400 response code and error message for invalid teacher email input', () => {
      return request(app)
      .post("/api/retrievefornotifications")
      .send({
        "teacher":  {a:1},
        "notification": "Hello everyone !"
      })
      .then(response => {
          expect(response.statusCode).toBe(400);
          expect(response.body.message).toBe('Invalid request');
      })
  });
  test('Should return 400 response code and error message for invalid notification message input', () => {
      return request(app)
      .post("/api/retrievefornotifications")
      .send({
        "teacher":  "teacherken@example.com",
        "notification": 2000
      })
      .then(response => {
          expect(response.statusCode).toBe(400);
          expect(response.body.message).toBe('Invalid request');
      })
  });
});
