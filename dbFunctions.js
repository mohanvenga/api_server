const { Teacher, Student, StudentTeacher, Sequelize } = require('./database');
const Op = Sequelize.Op

function register(teacherEmail, studentEmails){
    const register = Teacher.findOrCreate({
        where: {email: teacherEmail}
    })
    .spread((teacher, created) => teacher)
    .then(teacher => {
      const students = studentEmails.map(email => Student.findOrCreate({where: {email:email}})
          .spread((student, created)=> student));
      Promise.all(students)
      .then(_students => teacher.addStudents(_students))
    });
    return register;
}

function getCommonStudents(teacherEmails){
  const teacher_email_objs = teacherEmails.map(email=>{
    const obj = {};
    obj.email = email;
    return obj;
  });

  const getCommonStudentEmails = Teacher.findAll({
    attributes: ['id'],
    where: {[Op.or]: teacher_email_objs}
  })
  .then(teachers=>{
    const teacher_id_objs = teachers.map(teacher=>{
      const obj = {}
      obj.teacherId = teacher.id
      return obj;
    })
    return teacher_id_objs
  })
  .then(teacher_id_objs=>{
    const rows = StudentTeacher.findAll({
      attributes: ['studentId'],
      where: {[Op.or]: teacher_id_objs}
    });
    return rows
  })
  .then(rows=>{
    // create student id set
    const student_ids_list = rows.map(row=>row.studentId)
    const student_id_set = new Set(student_ids_list);
    const common_list = []
    // compare first and last indexes to common ids.
    student_id_set.forEach(id=>{
      const start_index = student_ids_list.indexOf(id);
      const back_index = student_ids_list.lastIndexOf(id);
      if (start_index !== back_index) {
        common_list.push(id)
      }
    })

    const studentIdObjs = common_list.map(id=>{
      const obj = {}
      obj.id = id
      return obj;  
    });
    return studentIdObjs;
  })
  .then(studentIdObjs=>{
    const students = Student.findAll({
      attributes: ['email'],
      where: {[Op.or]: studentIdObjs}
    })
    return students;
  })
  .then(students=>{
    const student_emails = students.map(student=>student.email);
    return student_emails;
  })
  return getCommonStudentEmails;
}

function suspendStudent(studentEmail){
  const suspend = Student.update({
    suspended: true
  },{
    where: {email: studentEmail}
  });
  return suspend;
}

function getRecipients(mentionedEmails, teacherEmail){
  const recipients = Teacher.findOne({
    where: {email: teacherEmail}
  })
  .then(teacher=>{
      const obj = {}
      obj.teacherId = teacher.id
      return obj;
  })
  .then(teacherIdObj=>{
    const rows = StudentTeacher.findAll({
      attributes: ['studentId'],
      where: teacherIdObj
    });
    return rows
  })
  .then(rows=>{
    const studentIdList = rows.map(row=>row.studentId);
    const mentionStudentEmailObjList = mentionedEmails.map(email=>{
      const obj = {};
      obj.email = email;
      return obj;
    });

    const idList = Student.findAll({
        attributes: ['id'],
        where: {[Op.or]: mentionStudentEmailObjList}
    })
    .then(mentionedStudents=>{
      const id_list = mentionedStudents.map(student=>student.id);
      return id_list;
    })
    .then(id_list=> {
      const list = studentIdList.concat(id_list)
      return list
    })
    return idList
  })
  .then(idList=>{
    const student_id_set = new Set(idList);
    const student_ids = [...student_id_set];
    const student_id_objs = student_ids.map(id=>{
      const obj = {}
      obj.id = id;
      return obj
    });

    const nonSuspendedEmails = Student.findAll({
      attributes: ['email'],
      where: {[Op.or]: student_id_objs, suspended: false}
    })
    .then(students=>{
      const email_list = students.map(student=>student.email);
      return email_list;
    })
    return nonSuspendedEmails
  })
  return recipients;
}

module.exports = {
    register,
    getCommonStudents,
    suspendStudent,
    getRecipients
};
