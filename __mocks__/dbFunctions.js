const register = jest.fn(()=> {
  const promise1 = Promise.resolve();
  return promise1;
})

const getCommonStudents = jest.fn(()=> {
  const promise1 = Promise.resolve(
    ['red@example.com', 'blue@example.com']
    );
  return promise1;
})

const suspendStudent = jest.fn(()=> {
  const promise1 = Promise.resolve();
  return promise1;
})

const getRecipients = jest.fn(()=> {
  const promise1 = Promise.resolve(
    ['apple@example.com', 'orange@example.com']
    );
  return promise1;
})

module.exports = {
    register,
    getCommonStudents,
    suspendStudent,
    getRecipients
};
