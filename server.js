const app = require('./app');

const { testConnection } = require('./database');
testConnection();

// start api server.
app.listen(8080, () => console.log('Example app listening on port 8080!'));