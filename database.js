const Sequelize = require('sequelize')
const TeacherModel = require('./models/teacher')
const StudentModel = require('./models/student')

const sequelize = new Sequelize('test_db', 'mohanvenga', 'password', {
  host: 'db',
  dialect: 'mysql'
});

const StudentTeacher = sequelize.define('student_teacher', {});
const Teacher = TeacherModel(sequelize, Sequelize)
const Student = StudentModel(sequelize, Sequelize)

const testConnection = function(){
  sequelize
    .authenticate()
    .then(() => {
      console.log('Database Connection has been established successfully.');
      sequelize.sync({ force: true })
        .then(() => {
          console.log(`Database & tables created!`)
        })
    })
    .then(()=>{
      // create student teacher association
    Student.belongsToMany(Teacher, {through: StudentTeacher});
    Teacher.belongsToMany(Student, {through: StudentTeacher});
    })
    .catch(err => {
      console.error('Unable to connect to the database, will try again in 5 seconds:', err);
      setTimeout(testConnection, 5000);
    });
}

module.exports = {
  Teacher,
  Student,
  StudentTeacher,
  testConnection,
  Sequelize
}
