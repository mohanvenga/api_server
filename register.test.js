const request = require('supertest');
const _ = require('lodash');
jest.mock('./dbFunctions');
const { register } = require('./dbFunctions');
const app = require('./app')

describe('Test /api/register', () => {
  test('Should return 204 response code and empty response body', () => {
      return request(app)
      .post("/api/register")
      .send({
        "teacher": "teacherken@gmail.com",
        "students":
          [
            "studentjon@example.com",
            "studenthon@example.com"
          ]
      })
      .then(response => {
          expect(response.statusCode).toBe(204);
          expect(_.isEmpty(response.body)).toBe(true);
      })
  });
  test('Should handle internal server db function errors', ()=> {
    register.mockImplementation(() => {
      const promise1 = new Promise(function(resolve, reject) {
        throw 'Uh-oh!';
      });
      return promise1;
    });
    return request(app)
    .post("/api/register")
    .send({
        "teacher": "teacherken@gmail.com",
        "students":
          [
            "studentjon@example.com",
            "studenthon@example.com"
          ]
      })
    .then(response => {
        expect(response.statusCode).toBe(500);
        expect(response.body.message).toEqual('Processing error. Please try again later.');
      })
  });
  test('Should return 400 error status and message for invalid teacher input', ()=> {
    return request(app)
    .post("/api/register")
    .send({
        "teacher": 1,
        "students":
          [
            "studentjon@example.com",
            "studenthon@example.com"
          ]
      })
    .then(response => {
        expect(response.statusCode).toBe(400);
        expect(response.body.message).toEqual('Invalid request');
      })
  });
  test('Should return 400 error status and message for invalid student input', ()=> {
    return request(app)
    .post("/api/register")
    .send({
        "teacher": "kevin@example.com",
        "students": 'joey@example.com'
      })
    .then(response => {
        expect(response.statusCode).toBe(400);
        expect(response.body.message).toEqual('Invalid request');
      })
  });
});