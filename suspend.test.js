const request = require('supertest');
const _ = require('lodash');
jest.mock('./dbFunctions');
const { suspendStudent, getRecipients } = require('./dbFunctions');
const app = require('./app')

describe('Test /api/suspend', () => {
  test('Should return 204 response code and empty response body', () => {
      return request(app)
      .post("/api/suspend")
      .send({
        "student" : "studentjon@example.com"
      })
      .then(response => {
          expect(response.statusCode).toBe(204);
          expect(_.isEmpty(response.body)).toBe(true);
      })
  });
  test('Should return 400 response code and error message for invalid input', () => {
      return request(app)
      .post("/api/suspend")
      .send({
        "student" : 100
      })
      .then(response => {
          expect(response.statusCode).toBe(400);
          expect(response.body.message).toEqual('Invalid request');
      })
  });
  test('Should handle internal server db function errors', () => {
    suspendStudent.mockImplementation(() => {
      const promise1 = new Promise(function(resolve, reject) {
        throw 'Uh-oh!';
      });
      return promise1;
    });
      return request(app)
      .post("/api/suspend")
      .send({
        "student" : "test@example.com"
      })
      .then(response => {
          expect(response.statusCode).toBe(500);
          expect(response.body.message).toEqual('Processing error. Please try again later.');
      })
  });
});