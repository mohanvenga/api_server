# README #

You need to be able to run docker-compose and support docker-compose file version 3.2
Please install the minimum Docker version required.

The docker images for the application and database are hosted on Docker Hub.
Please make sure you are able to access and download docker images hosted on Docker Hub.

### INSTRUCTIONS ###

1. Open terminal and cd into the project root directory.
2. Run `docker-compose up -d`
3. The application and database servers should be running locally on your computer.
4. The application server will be running on localhost:8080
