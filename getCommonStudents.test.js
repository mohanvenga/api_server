const request = require('supertest');
const _ = require('lodash');
jest.mock('./dbFunctions');
const { getCommonStudents } = require('./dbFunctions');
const app = require('./app')

describe('Test /api/commonstudents', () => {
  test('Should return 200 response code and students array for set of teacher emails', () => {
      return request(app)
      .get("/api/commonstudents?teacher=teacherken%40example.com&teacher=teacherjoe%40example.com")
      .then(response => {
          expect(response.statusCode).toBe(200);
          expect(_.isArray(response.body.students)).toBe(true);
      })
  });
  test('Should return 200 response code and students array for one teacher email', () => {
      return request(app)
      .get("/api/commonstudents?teacher=teacherken%40example.com")
      .then(response => {
          expect(response.statusCode).toBe(200);
          expect(_.isArray(response.body.students)).toBe(true);
      })
  });
  test('Should handle internal server db function errors', ()=> {
    getCommonStudents.mockImplementation(() => {
      const promise1 = new Promise(function(resolve, reject) {
        throw 'Uh-oh!';
      });
      return promise1;
    });
    return request(app)
    .get("/api/commonstudents?teacher=teacherken%40example.com")
    .then(response => {
        expect(response.statusCode).toBe(500);
        expect(response.body.message).toEqual('Processing error. Please try again later.');
      })
  });
});