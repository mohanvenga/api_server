var express = require('express')
var router = express.Router()
const _ = require('lodash');
const { register, getCommonStudents, suspendStudent, getRecipients } = require('./dbFunctions');

router.post('/register', (req, res) => {
  const teacher = req.body.teacher;
  const students = req.body.students;

  if (typeof teacher !== 'string' || !_.isArray(students)) {
    res.status(400).json({message:'Invalid request'});
    return
  }

  register(teacher, students)
  .then(()=>{
    res.status(204).end();
  })
  .catch(err=>res.status(500).json({message:'Processing error. Please try again later.'}));
});

router.get('/commonstudents', (req,res)=> {
	const teachers = req.query.teacher;
  var teacherList = []

  if (_.isArray(teachers)) {
    teacherList = teachers;
  } else if (typeof teachers === 'string') {
    teacherList.push(teachers);
  }

	getCommonStudents(teacherList)
	.then(students=>{
		res.status(200).json({students});
	})
  .catch(err=>res.status(500).json({message:'Processing error. Please try again later.'}));
});

router.post('/suspend', (req, res) => {
  const studentEmail = req.body.student;

  if (typeof studentEmail !== 'string'){
    res.status(400).json({message:'Invalid request'});
    return
  }

  suspendStudent(studentEmail)
  .then(()=>{
    res.status(204).end()
  })
  .catch(err=>res.status(500).json({message:'Processing error. Please try again later.'}));
});

router.post('/retrievefornotifications', (req, res) => {

  const teacherEmail = req.body.teacher;
  const notification = req.body.notification;

  if (typeof teacherEmail !== 'string' || typeof notification !== 'string') {
    res.status(400).json({message:'Invalid request'})
    return
  }

  const regex = /@\S+/g
  const found = notification.match(regex);

  var mentionedEmails = [];

  if (found !== null) {
    mentionedEmails = found.map(match=>{
      const sliced = match.slice(1);
      return sliced
    });
  }

  getRecipients(mentionedEmails, teacherEmail)
  .then((recipients)=>{
    res.status(200).json({recipients});
  })
  .catch(err=>res.status(500).json({message:'Processing error. Please try again later.'}));
});



module.exports = router