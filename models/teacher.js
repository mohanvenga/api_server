
module.exports = (sequelize, types) => {
  return sequelize.define('teacher', {
    email: types.TEXT
  });
}