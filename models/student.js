
module.exports = (sequelize, types) => {
  return sequelize.define('student', {
    email: types.TEXT,
    suspended: { type: types.BOOLEAN, allowNull: false, defaultValue: false }
  });
}
